public class Courier {
    private int courierID;
    private String courierName;
    private String courierFamily;
    private String courierPhone;

    public Courier(int courierID, String courierName, String courierFamily, String courierPhone) {
        this.courierID = courierID;
        this.courierName = courierName;
        this.courierFamily = courierFamily;
        this.courierPhone = courierPhone;
    }

    public int getCourierID() {
        return courierID;
    }

    public String getCourierFamily() {
        return courierFamily;
    }

    public String getCourierName() {
        return courierName;
    }

    public String getCourierPhone() {
        return courierPhone;
    }

}
