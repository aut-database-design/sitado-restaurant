public class Market {
    private int marketId;
    private String marketName;
    private String marketPhone;
    private boolean marketStatus;

    public Market(int marketId, String marketName, String marketPhone, boolean marketStatus) {
        this.marketId = marketId;
        this.marketName = marketName;
        this.marketPhone = marketPhone;
        this.marketStatus = marketStatus;
    }

    public int getMarketId() {
        return marketId;
    }

    public String getMarketName() {
        return marketName;
    }

    public String getMarketPhone() {
        return marketPhone;
    }

    public boolean isMarketStatus() {
        return marketStatus;
    }
}
