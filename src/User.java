public class User {
    private int userID;
    private String userName;
    private String userFamily;
    private String userPhone;
    private int userAge;

    public User(int userId,String userName, String userFamily, String userPhone, int userAge){
        this.userID = userId;
        this.userName = userName;
        this.userFamily = userFamily;
        this.userPhone = userPhone;
        this.userAge = userAge;
    }

    public int getUserID() {
        return userID;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserFamily() {
        return userFamily;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public int getUserAge() {
        return userAge;
    }
}
