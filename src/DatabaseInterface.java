import com.sun.jdi.ArrayReference;
import javafx.util.Pair;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;

public class DatabaseInterface{
    private final String USERNAME = "root";
    private final String PASSWORD = "26262626";
    private final String HOST ="jdbc:mysql://localhost:3306/database_design_final";
    private ResultSet resultSet;
    private Connection connection;
    private Statement statement;
    PreparedStatement preparedStatement;

    public void makeConnection(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            try {
                connection = DriverManager.getConnection(HOST,USERNAME,PASSWORD);
            }
            catch (Exception ex){
                createDatabase();
                connection = DriverManager.getConnection(HOST,USERNAME,PASSWORD);
            }

            statement = connection.createStatement();
            System.out.println("Database connected successfully");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "امکان اتصال به پایگاه داده وجود ندارد!",
                    "خطا", JOptionPane.ERROR_MESSAGE, null);

            e.printStackTrace();
        }
    }

    public void closeConnection(){
        if(connection!=null){
            try {
                connection.close();
                System.out.println("Database disconnected successfully");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(statement!=null){
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void createDatabase() throws Exception{
        statement.executeUpdate(DatabaseMigrationCode.createDatabaseCode());
    }

    public void removeDatabase() throws Exception{
        statement.execute(DatabaseMigrationCode.dropDatabaseCode());
    }

    public void removeLogs() throws Exception{
        statement.executeQuery(" CALL removeOldLog();");
    }

    public int sumPricesUser(int user_ID) throws Exception{
        preparedStatement = connection.prepareStatement(
            "WITH properItem(iid,inumber) as "+
                    "(SELECT factorItems.item_ID , factorItems.userFactor_Item_number " +
                    "FROM UserFactor as factros, UserFactor_Item as factorItems " +
                    "WHERE factros.user_ID = ? AND factros.userFactor_ID = factorItems.userFactor_ID )" +
                 "SELECT SUM(properItem.inumber * Item.item_price)" +
                    " FROM properItem, Item" +
                    " WHERE properItem.iid = Item.item_ID"
        );

        preparedStatement.setInt(1,user_ID);
        resultSet = preparedStatement.executeQuery();
        if (resultSet.next()){
            return resultSet.getInt(1);
        }
        throw new Exception();
    }

    public String bestNameUserFactor(int user_ID) throws Exception{
        preparedStatement = connection.prepareStatement(
                "WITH properItem(iid,inumber) as "+
                        "(SELECT factorItems.item_ID , factorItems.userFactor_Item_number " +
                        "FROM UserFactor as factros, UserFactor_Item as factorItems " +
                        "WHERE factros.user_ID = ? AND factros.userFactor_ID = factorItems.userFactor_ID )" +
                     ", properItem2(iname,inumber) as " +
                        "(SELECT items.item_name, properItem.inumber" +
                        " FROM properItem,Item as items " +
                        " WHERE properItem.iid = items.item_ID) " +
                     "SELECT  iname,sum(inumber) as sum " +
                        "FROM properItem2 " +
                        "GROUP BY iname " +
                        "ORDER BY sum desc"
        );

        preparedStatement.setInt(1,user_ID);
        resultSet = preparedStatement.executeQuery();
        if (resultSet.next()){
            return resultSet.getString(1);
        }
        throw new Exception();
    }

    public ArrayList<Pair<Integer,Integer>> managerReportUser() throws Exception{
        ArrayList<Integer> userFactors = userFactorIDToday();
        ArrayList<Pair<Integer,Integer>> output = new ArrayList<>();
        for (Integer userFactor_ID : userFactors){
            int sum = sumPriceUserFactor(userFactor_ID);
            output.add(new Pair<>(userFactor_ID,sum));
        }
        return output;
    }

    public ArrayList<Pair<Integer,Integer>> managerReportMarket() throws Exception {
        ArrayList<Integer> marketFactors = marketFactorIDToday();
        ArrayList<Pair<Integer, Integer>> output = new ArrayList<>();
        for (Integer marketFactor_ID : marketFactors) {
            int sum = sumPriceMarketFactor(marketFactor_ID);
            output.add(new Pair<>(marketFactor_ID, sum));
        }
        return output;
    }

    public ArrayList<Integer> marketFactorIDToday() throws Exception{
        resultSet = statement.executeQuery("SELECT marketFactor_ID FROM MarketFactor WHERE DATE(marketFactor_date) = CURDATE()");
        ArrayList<Integer> marketFactoryIDs = new ArrayList<>();
        while (resultSet.next()){
            marketFactoryIDs.add(resultSet.getInt(1));
        }
        return marketFactoryIDs;
    }

    public ArrayList<Integer> userFactorIDToday() throws Exception{
        resultSet = statement.executeQuery("SELECT userFactor_ID FROM UserFactor WHERE DATE(userFactor_data) = CURDATE()");
        ArrayList<Integer> userFactoryIDs = new ArrayList<>();
        while (resultSet.next()){
            userFactoryIDs.add(resultSet.getInt(1));
        }
        return userFactoryIDs;
    }

    public int sumPriceUserFactor(int userFactor_ID) throws Exception {
        preparedStatement = connection.prepareStatement(
                "SELECT SUM(items.item_price * factors.userFactor_Item_number) " +
                        "FROM UserFactor_Item as factors,Item as items" +
                        " WHERE factors.userFactor_ID = ? AND factors.item_ID = items.item_ID"
        );

        preparedStatement.setInt(1, userFactor_ID);
        resultSet = preparedStatement.executeQuery();
        if (resultSet.next()){
            return resultSet.getInt(1);
        }
        throw new Exception();
    }

    public int sumPriceMarketFactor(int marketFactor_ID) throws Exception{
        preparedStatement = connection.prepareStatement(
                "SELECT SUM(items.item_price * factors.marketFactor_Item_number) " +
                        "FROM marketFactor_Item as factors,Item as items" +
                        " WHERE factors.marketFactor_ID = ? AND factors.item_ID = items.item_ID"
        );

        preparedStatement.setInt(1, marketFactor_ID);
        resultSet = preparedStatement.executeQuery();
        if (resultSet.next()){
            return resultSet.getInt(1);
        }
        throw new Exception();
    }

    public ArrayList<Pair<String,Integer>> menuItems() throws Exception{
        ArrayList<Pair<String,Integer>> menuItems = new ArrayList<>();
         resultSet = statement.executeQuery(
                "SELECT items.item_name,items.item_price " +
                        "FROM Menu as menuItems,Item as items " +
                        "WHERE menuItems.item_ID = items.item_ID " +
                        "ORDER BY menuItems.position"
        );

         while (resultSet.next()){
             menuItems.add(new Pair<>(resultSet.getString(1),resultSet.getInt(2)));
         }
         return menuItems;
    }

    public void setMenuItems(ArrayList<Pair<String,Integer>> menuItems) throws Exception{
        removeAllMenu();
        for (int i=0 ; i<menuItems.size() ; i++){
            String item_name = menuItems.get(i).getKey();
            int item_price = menuItems.get(i).getValue();
            addItem(item_name,item_price);
            int item_ID = findItem(item_name,item_price);
            addItemToMenu(item_ID);
        }
    }

    public boolean addItemToMenu(int item_ID){
        try {
            preparedStatement = connection.prepareStatement(
                    "INSERT into Menu(item_ID)" +
                            " values(?)");

            preparedStatement.setInt(1,item_ID);

            int rowEffects = preparedStatement.executeUpdate();
            System.out.println(rowEffects + " records inserted!");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }


    public void addItem(String item_name, int item_price) throws Exception{
        try {
            findItem(item_name,item_price);
            System.out.println("Item find at now!");
        }
        catch (Exception ex){
            preparedStatement = connection.prepareStatement(
                    "INSERT into Item(item_name,item_price)" +
                            " values(?,?)");

            preparedStatement.setString(1,item_name);
            preparedStatement.setInt(2,item_price);

            int rowEffects = preparedStatement.executeUpdate();
            System.out.println(rowEffects + " records inserted!");

        }

    }

    public int findItem(String item_name, int item_price) throws Exception{
        preparedStatement = connection.prepareStatement(
                "SELECT item_ID "
                        +"FROM Item "
                        +"WHERE item_name = ? AND item_price = ? "
        );

        preparedStatement.setString(1,item_name);
        preparedStatement.setInt(2,item_price);
        resultSet = preparedStatement.executeQuery();
        if(resultSet.next()){
            return  resultSet.getInt(1);
        }
        throw new Exception();
    }

//    public boolean removeItem(int item_ID){
//        try {
//            preparedStatement = connection.prepareStatement(
//                    "DELETE FROM Item "
//                            +"WHERE item_ID= ?"
//            );
//
//            preparedStatement.setInt(1,item_ID);
//            int rowEffects = preparedStatement.executeUpdate();
//            System.out.println(rowEffects + " records inserted!");
//            return true;
//        } catch (SQLException e) {
//            e.printStackTrace();
//            return false;
//        }
//    }

    public void removeAllMenu() throws Exception{
        int rowEffects = statement.executeUpdate("DELETE FROM Menu");
        System.out.println(rowEffects + " records deleted");
    }

    public void buyMarket(int market_ID, ArrayList<String> itemNames,
                          ArrayList<Integer> itemNumbers, ArrayList<Integer> itemPrices) throws Exception{
        ArrayList<Integer> item_IDs = item_IDsMarket(itemNames,itemPrices);
        addMarketFactor(market_ID);

        int marketFactor_ID = lastMarketFactor(market_ID);

        for (int i=0 ; i<itemNames.size() ; i++){
            addMarketFactor_Item(marketFactor_ID,item_IDs.get(i),itemNumbers.get(i));
        }
    }

    public void buyUser(int user_ID, String address_name, int courier_ID,
                    ArrayList<String> itemNames, ArrayList<Integer> itemNumbers) throws Exception{

        ArrayList<Integer> item_IDs = item_IDsMenu(itemNames);

        if (user_ID==-1){
           addUserFactor();
        }
        else if (address_name == null){
            addUserFactor(user_ID);
        }
        else {
            addUserFactor(user_ID,address_name,courier_ID);
        }

        int userFactor_ID = lastUserFactor();


        for (int i = 0 ; i<itemNames.size() ; i++){
            addUserFactor_Item(userFactor_ID,item_IDs.get(i),itemNumbers.get(i));
        }
    }

    public ArrayList<Integer> item_IDsMenu(ArrayList<String> itemNames) throws Exception{
        ArrayList<Integer> item_IDs = new ArrayList<>();
        for (String itemName : itemNames){
            int id = findItemMenu(itemName);
            item_IDs.add(id);
        }
        return item_IDs;
    }

    public ArrayList<Integer> item_IDsMarket(ArrayList<String> itemNames, ArrayList<Integer> itemPrices) throws Exception{
        ArrayList<Integer> itemIDs = new ArrayList<>();
        for (int i=0 ; i<itemNames.size() ; i++){
            addItem(itemNames.get(i),itemPrices.get(i));
            int id = findItem(itemNames.get(i),itemPrices.get(i));
            itemIDs.add(id);
        }
        return itemIDs;
    }

    public int findItemMenu(String item_name) throws Exception{
        preparedStatement = connection.prepareStatement(
            "SELECT  items.item_ID " +
                    "FROM Item as items,Menu as menuItems" +
                    " WHERE items.item_ID= menuItems.item_ID AND items.item_name = ?"
        );

        preparedStatement.setString(1,item_name);
        resultSet = preparedStatement.executeQuery();
        if (resultSet.next()){
            return resultSet.getInt(1);
        }
        throw new Exception();
    }

    public void addUserFactor_Item(int userFactor_ID, int item_ID, int userFactor_Item_number) throws Exception{
        preparedStatement = connection.prepareStatement(
                "INSERT into UserFactor_Item(userFactor_ID,item_ID,userFactor_Item_number)" +
                        " values(?,?,?)");

        preparedStatement.setInt(1,userFactor_ID);
        preparedStatement.setInt(2,item_ID);
        preparedStatement.setInt(3,userFactor_Item_number);


        int rowEffects = preparedStatement.executeUpdate();
        System.out.println(rowEffects + " records inserted!");
    }

    public int lastUserFactor() throws Exception{
        preparedStatement = connection.prepareStatement(
                "SELECT userFactor_ID "
                        +"FROM UserFactor "
                        +"ORDER BY userFactor_ID desc"
        );

        resultSet = preparedStatement.executeQuery();
        if(resultSet.next()) {
            return resultSet.getInt(1);
        }
        throw new Exception();
    }

    private void  addUserFactor() throws Exception{
        preparedStatement = connection.prepareStatement(
                "INSERT into UserFactor (user_ID,address_name,courier_ID)" +
                        " values(NULL,NULL,NULL)");

        int rowEffects = preparedStatement.executeUpdate();
        System.out.println(rowEffects + " records inserted");
    }

    private void addUserFactor(int user_ID) throws Exception{
        preparedStatement = connection.prepareStatement(
                "INSERT into UserFactor (user_ID,address_name,courier_ID)" +
                        " values(?,NULL,NULL)");

        preparedStatement.setInt(1,user_ID);
        int rowEffects = preparedStatement.executeUpdate();
        System.out.println(rowEffects + " records inserted");
    }

    private void addUserFactor(int user_ID, String address_name, int courier_ID) throws Exception{
        preparedStatement = connection.prepareStatement(
                "INSERT into UserFactor (user_ID,address_name,courier_ID)" +
                        " values(?,?,?)");

        System.out.println(user_ID+" "+address_name+" " + courier_ID);
        preparedStatement.setInt(1,user_ID);
        preparedStatement.setString(2,address_name);
        preparedStatement.setInt(3,courier_ID);
        int rowEffects = preparedStatement.executeUpdate();
        System.out.println(rowEffects + " records inserted");
    }

    public void addMarketFactor_Item(int marketFactor_ID, int item_ID, int marketFactor_Item_number) throws Exception{
        preparedStatement = connection.prepareStatement(
                "INSERT into MarketFactor_Item(marketFactor_ID,item_ID,marketFactor_Item_number)" +
                        " values(?,?,?)");

        preparedStatement.setInt(1,marketFactor_ID);
        preparedStatement.setInt(2,item_ID);
        preparedStatement.setInt(3,marketFactor_Item_number);

        int rowEffects = preparedStatement.executeUpdate();
        System.out.println(rowEffects + " records inserted!");

    }

    public int lastMarketFactor(int market_ID) throws Exception{
        preparedStatement = connection.prepareStatement(
                "SELECT marketFactor_ID "
                        +"FROM MarketFactor "
                        +"WHERE market_ID = ? "
                        +"ORDER BY marketFactor_ID desc"
        );

        preparedStatement.setInt(1,market_ID);
        resultSet = preparedStatement.executeQuery();
        if(resultSet.next()){
            return  resultSet.getInt(1);
        }
        throw new Exception();
    }

    public boolean addMarketFactor(int market_ID){
        try {
            preparedStatement = connection.prepareStatement(
                    "INSERT into MarketFactor(market_ID)" +
                            " values(?)");

            preparedStatement.setInt(1,market_ID);

            int rowEffects = preparedStatement.executeUpdate();
            System.out.println(rowEffects + " records inserted!");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void updateMarket(Market market) throws Exception{
        preparedStatement = connection.prepareStatement(
                "UPDATE Market "
                +"SET market_name = ?, market_phone = ?, market_status = ? "
                +"WHERE market_ID = ?"
        );

        preparedStatement.setString(1,market.getMarketName());
        preparedStatement.setString(2,market.getMarketPhone());
        preparedStatement.setBoolean(3,market.isMarketStatus());
        preparedStatement.setInt(4,market.getMarketId());

        int rowEffects = preparedStatement.executeUpdate();
        System.out.println(rowEffects + " records updated");
    }

    public void addMarket(Market market) throws Exception{
        preparedStatement = connection.prepareStatement(
                "INSERT into Market(market_ID,market_name,market_phone,market_status)" +
                        " values(?,?,?,?)");

        preparedStatement.setInt(1,market.getMarketId());
        preparedStatement.setString(2,market.getMarketName());
        preparedStatement.setString(3,market.getMarketPhone());
        preparedStatement.setBoolean(4,market.isMarketStatus());

        int rowEffects = preparedStatement.executeUpdate();
        System.out.println(rowEffects + " records inserted");
    }

    public Market market(int market_ID) throws Exception{
        preparedStatement = connection.prepareStatement(
                "SELECT market_ID,market_name,market_phone,market_status" +
                        " FROM Market " +
                        " WHERE market_ID = ?"
        );

        preparedStatement.setInt(1,market_ID);
        resultSet = preparedStatement.executeQuery();
        if (resultSet.next()){
            return new Market(resultSet.getInt(1),resultSet.getString(2),
                    resultSet.getString(3),resultSet.getBoolean(4));
        }
        throw new Exception();
    }

    public ArrayList<Integer> allMarketID() throws Exception{
        resultSet = statement.executeQuery("SELECT market_ID FROM Market WHERE market_Status = true");
        ArrayList<Integer> marketIDs = new ArrayList<>();
        while (resultSet.next()){
            marketIDs.add(resultSet.getInt(1));
        }
        return marketIDs;
    }

    public Courier courier(int courier_ID) throws Exception{
        preparedStatement = connection.prepareStatement(
                "SELECT courier_ID, courier_name, courier_family, courier_phone " +
                        "FROM Courier " +
                        "WHERE courier_ID = ?"
        );

        preparedStatement.setInt(1,courier_ID);
        resultSet = preparedStatement.executeQuery();

        if (resultSet.next()){
            return new Courier(resultSet.getInt(1),resultSet.getString(2)
                    ,resultSet.getString(3),resultSet.getString(4));
        }
        throw new Exception();
    }

    public void addCourier(Courier courier) throws Exception{
        preparedStatement = connection.prepareStatement(
                "INSERT into Courier(courier_ID,courier_name,courier_family,courier_phone)" +
                        " values(?,?,?,?)");

        preparedStatement.setInt(1,courier.getCourierID());
        preparedStatement.setString(2,courier.getCourierName());
        preparedStatement.setString(3,courier.getCourierFamily());
        preparedStatement.setString(4,courier.getCourierPhone());

        int rowEffects = preparedStatement.executeUpdate();
        System.out.println(rowEffects + " records inserted");
    }

    public void updateCourier(Courier courier) throws Exception{
        preparedStatement = connection.prepareStatement(
                "UPDATE Courier " +
                        "SET courier_name = ?, courier_family=?, courier_phone=? " +
                        "WHERE courier_ID = ? "
        );

        preparedStatement.setString(1,courier.getCourierName());
        preparedStatement.setString(2,courier.getCourierFamily());
        preparedStatement.setString(3,courier.getCourierPhone());
        preparedStatement.setInt(4,courier.getCourierID());

        int rowEffects = preparedStatement.executeUpdate();
        System.out.println(rowEffects + "records updated");
    }

    public void removeCourier(int courier_ID) throws Exception{
        preparedStatement = connection.prepareStatement(
                "DELETE FROM Courier "
                        +"WHERE courier_ID = ?"
        );
        preparedStatement.setInt(1,courier_ID);
        int rowEffects = preparedStatement.executeUpdate();
        System.out.println(rowEffects + " records deleted");
    }

    public ArrayList<Integer> allCourierID(){
        try {
            resultSet = statement.executeQuery("SELECT courier_ID FROM Courier");
            ArrayList<Integer> courierID = new ArrayList<>();
            while (resultSet.next()){
                courierID.add(resultSet.getInt(1));
            }
            return courierID;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void updateAddress(Address address) throws Exception {
        preparedStatement = connection.prepareStatement(
                "UPDATE Address" +
                        " SET  address_location = ? , address_phone = ? " +
                        " WHERE user_ID = ? AND address_name = ?"
        );

        preparedStatement.setString(1,address.getAddressLocation());
        preparedStatement.setString(2,address.getAddressPhone());
        preparedStatement.setInt(3,address.getUserID());
        preparedStatement.setString(4,address.getAddressName());

        int rowEffects = preparedStatement.executeUpdate();
        System.out.println(rowEffects + " records update");
    }

    public void addAddress(String address_name, String address_location, String address_phone, int user_ID) throws Exception {
        preparedStatement = connection.prepareStatement(
                "INSERT into Address(address_name,address_location,address_phone,user_ID)" +
                        " values(?,?,?,?)");

        preparedStatement.setString(1,address_name);
        preparedStatement.setString(2,address_location);
        preparedStatement.setString(3,address_phone);
        preparedStatement.setInt(4,user_ID);

        int rowEffects = preparedStatement.executeUpdate();
        System.out.println(rowEffects + " records inserted!");
    }

    public void removeAddress(int user_ID, String address_name) throws Exception {
        preparedStatement = connection.prepareStatement(
                "DELETE FROM Address "
                        +"WHERE user_ID = ? AND address_name = ?"
        );

        preparedStatement.setInt(1,user_ID);
        preparedStatement.setString(2,address_name);
        int rowEffects = preparedStatement.executeUpdate();
        System.out.println(rowEffects + " records deleted");
    }

    public ArrayList<Address> allAddress(int user_ID) throws SQLException {
        preparedStatement = connection.prepareStatement(
                "SELECT  user_ID,address_name, address_location, address_phone" +
                        " FROM Address WHERE user_ID = ?"
        );

        preparedStatement.setInt(1,user_ID);
        resultSet = preparedStatement.executeQuery();

        ArrayList<Address> addresses = new ArrayList<>();
        while (resultSet.next()){
            addresses.add(new Address(resultSet.getInt(1),resultSet.getString(2),
                    resultSet.getString(3),resultSet.getString(4)));
        }
//        Address[] output = new Address[addresses.size()];
//        for(int i=0 ; i<addresses.size() ; i++){
//            output[i] = addresses.get(i);
//        }
        return addresses;
    }

    public Address address(int user_ID, String address_name) throws Exception {
        preparedStatement = connection.prepareStatement(
                "SELECT user_ID,address_name, address_location, address_phone " +
                        "FROM Address " +
                        "WHERE user_ID = ? AND address_name = ?"
        );

        preparedStatement.setInt(1,user_ID);
        preparedStatement.setString(2,address_name);

        resultSet = preparedStatement.executeQuery();
        if (resultSet.next()){
                return new Address(resultSet.getInt(1),resultSet.getString(2),
                        resultSet.getString(3),resultSet.getString(4));
        }
        throw new Exception();
    }

    public boolean updateUser(User user){
        try {
            preparedStatement = connection.prepareStatement(
              "UPDATE User "
                    +"SET user_name = ?, user_family = ?, user_phone = ?, user_age =  ? "
                    +"WHERE user_ID = ?"
            );
            preparedStatement.setString(1,user.getUserName());
            preparedStatement.setString(2,user.getUserFamily());
            preparedStatement.setString(3,user.getUserPhone());
            preparedStatement.setInt(4,user.getUserAge());
            preparedStatement.setInt(5,user.getUserID());

            int rowEffects = preparedStatement.executeUpdate();
            System.out.println(rowEffects + " records updated");

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }


    }

    public User user(int user_ID){
        try {
            preparedStatement = connection.prepareStatement(
                    "SELECT user_ID, user_name, user_family, user_phone, user_age "
                            +"FROM User "
                            +"WHERE user_ID = ? "
            );

            preparedStatement.setInt(1,user_ID);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                return new User(resultSet.getInt(1),resultSet.getString(2),
                        resultSet.getString(3),resultSet.getString(4),resultSet.getInt(5));

            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Integer[] allUserID(){
        try {
            resultSet = statement.executeQuery("SELECT user_ID FROM User");
            ArrayList<Integer> userID = new ArrayList<Integer>();
            while (resultSet.next()) {
                userID.add(resultSet.getInt(1));
            }
            Integer[] output = new Integer[userID.size()];
            int i = 0 ;
            for (Integer id : userID){
                output[i] = id;
                i++;
            }
            return output;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean removeUser(int user_ID){
        try {
            preparedStatement = connection.prepareStatement(
                    "DELETE FROM User "
                    +"WHERE user_id = ?"
            );

            preparedStatement.setInt(1,user_ID);
            int rowEffects = preparedStatement.executeUpdate();
            System.out.println(rowEffects + " records deleted!");


            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean addUser(User user){
        try {
             preparedStatement = connection.prepareStatement(
                    "INSERT into user(user_ID,user_name,user_family,user_phone,user_age)" +
                    " values(?,?,?,?,?)");

             preparedStatement.setInt(1,user.getUserID());
             preparedStatement.setString(2,user.getUserName());
             preparedStatement.setString(3,user.getUserFamily());
             preparedStatement.setString(4,user.getUserPhone());
             preparedStatement.setInt(5,user.getUserAge());

             int rowEffects = preparedStatement.executeUpdate();
            System.out.println(rowEffects + " records inserted!");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }

}
