import java.sql.PreparedStatement;

public class DatabaseMigrationCode {
    public static String dropDatabaseCode(){
        return  " drop database database_design_final; \n";

//        return "SELECT concat('DROP TABLE IF EXISTS `', table_name, '`;')\n" +
//                "FROM information_schema.tables\n" +
//                "WHERE table_schema = 'database_design_final';";

    }

    public static String createDatabaseCode(){
    return " create schema database_design_final;\n" +
        "Use database_design_final;\n" +
        "\n" +
        "create table Market\n" +
        "(\n" +
        "\tmarket_ID integer not null,\n" +
        "\tmarket_name varchar(30) not null,\n" +
        "\tmarket_phone varchar(11) not null ,\n" +
        "\tmarket_status boolean default true not null,\n" +
        "\tconstraint Market_pk\n" +
        "\t\tprimary key (market_ID)\n" +
        ");\n" +
        "\n" +
        "create table MarketFactor\n" +
        "(\n" +
        "\tmarketFactor_ID integer auto_increment,\n" +
        "\tmarketFactor_date datetime default current_timestamp,\n" +
        "\tmarket_ID integer not null,\n" +
        "\tconstraint MarketFactor_pk\n" +
        "\t\tprimary key (marketFactor_ID),\n" +
        "\tconstraint MarketFactor_market_market_ID_fk\n" +
        "\t\tforeign key (market_ID) references market (market_ID)\n" +
        "\t\tON DELETE CASCADE\n" +
        ");\n" +
        "\n" +
        "create table Item\n" +
        "(\n" +
        "\titem_ID integer auto_increment,\n" +
        "\titem_name varchar(30) not null,\n" +
        "\titem_price integer not null,\n" +
        "\tconstraint Item_pk\n" +
        "\t\tprimary key (item_ID)\n" +
        ");\n" +
        "\n" +
        "create table MarketFactor_Item\n" +
        "(\n" +
        "\tmarketFactor_ID integer not null,\n" +
        "\titem_ID integer not null,\n" +
        "\tmarketFactor_Item_number integer not null CHECK (marketFactor_Item_number>0),\n" +
        "\tconstraint MarketFactor_Item_pk\n" +
        "\t\tprimary key (marketFactor_ID, item_ID),\n" +
        "\tconstraint MarketFactor_Item_item_item_ID_fk\n" +
        "\t\tforeign key (item_ID) references item (item_ID)\n" +
        "        ON DELETE CASCADE,\n" +
        "\tconstraint MarketFactor_Item_marketfactor_marketFactor_ID_fk\n" +
        "\t\tforeign key (marketFactor_ID) references marketfactor (marketFactor_ID)\n" +
        "        ON DELETE CASCADE\n" +
        ");\n" +
        "\n" +
        "create table Menu\n" +
        "(\n" +
        "\tposition integer auto_increment,\n" +
        "\titem_ID integer not null,\n" +
        "\tconstraint Menu_pk\n" +
        "\t\tprimary key (position),\n" +
        "\tconstraint Menu_item_item_ID_fk\n" +
        "\t\tforeign key (item_ID) references item (item_ID)\n" +
        "        ON DELETE CASCADE\n" +
        ");\n" +
        "\n" +
        "create table User\n" +
        "(\n" +
        "\tuser_ID integer not null,\n" +
        "\tuser_name varchar(30) not null,\n" +
        "\tuser_family varchar(30) not null,\n" +
        "\tuser_phone varchar(11) not null,\n" +
        "\tuser_age integer not null,\n" +
        "\tconstraint User_pk\n" +
        "\t\tprimary key (user_ID)\n" +
        ");\n" +
        "\n" +
        "create table Address\n" +
        "(\n" +
        "\taddress_name varchar(30) not null,\n" +
        "\taddress_location varchar(100) not null,\n" +
        "\taddress_phone varchar(11) not null,\n" +
        "\tuser_ID integer not null,\n" +
        "\tconstraint Address_pk\n" +
        "\t\tprimary key (user_ID, address_name),\n" +
        "\tconstraint Address_user_user_ID_fk\n" +
        "\t\tforeign key (user_ID) references user (user_ID)\n" +
        "        ON DELETE CASCADE\n" +
        ");\n" +
        "\n" +
        "create table Courier\n" +
        "(\n" +
        "\tcourier_ID integer not null,\n" +
        "\tcourier_name varchar(30) not null,\n" +
        "\tcourier_family varchar(30) not null,\n" +
        "\tcourier_phone varchar(11) not null,\n" +
        "\tconstraint Courier_pk\n" +
        "\t\tprimary key (courier_ID)\n" +
        ");\n" +
        "\n" +
        "create table UserFactor\n" +
        "(\n" +
        "\tuserFactor_ID integer auto_increment,\n" +
        "\tuserFactor_data datetime default current_timestamp,\n" +
        "\tuser_ID integer default null null,\n" +
        "\taddress_name varchar(30) default null null,\n" +
        "\tcourier_ID integer default null null,\n" +
        "\tconstraint UserFactor_pk\n" +
        "\t\tprimary key (userFactor_ID),\n" +
        "\tconstraint UserFactor_address_fk\n" +
        "\t\tforeign key (user_ID,address_name) references address (user_ID,address_name)\n" +
        "        ON DELETE SET NULL ON UPDATE CASCADE,\n" +
        "\tconstraint UserFactor_courier_courier_ID_fk\n" +
        "\t\tforeign key (courier_ID) references courier (courier_ID)\n" +
        "\t\tON DELETE SET NULL,\n" +
        "\tconstraint UserFactor_user_user_ID_fk\n" +
        "\t\tforeign key (user_ID) references user (user_ID)\n" +
        "\t\tON DELETE SET NULL\n" +
        ");\n" +
        "\n" +
        "\n" +
        "\n" +
        "create table userFactor_Item\n" +
        "(\n" +
        "\tuserFactor_ID integer not null,\n" +
        "\titem_ID integer not null,\n" +
        "\tuserFactor_Item_number integer not null CHECK (userFactor_Item_number>0),\n" +
        "\tconstraint userFactor_Item_pk\n" +
        "\t\tprimary key (userFactor_ID, item_ID),\n" +
        "\tconstraint userFactor_Item_item_item_ID_fk\n" +
        "\t\tforeign key (item_ID) references item (item_ID)\n" +
        "        ON DELETE CASCADE,\n" +
        "\tconstraint userFactor_Item_userfactor_userFactor_ID_fk\n" +
        "\t\tforeign key (userFactor_ID) references userfactor (userFactor_ID)\n" +
        "\t\tON DELETE CASCADE\n" +
        ");\n" +
        "\n" +
        "\n" +
        "CREATE TRIGGER market_phone_validation BEFORE INSERT ON market\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        IF (NEW.market_phone  REGEXP '[0-9]{11}') = 0 THEN\n" +
        "            SIGNAL SQLSTATE '02000' SET MESSAGE_TEXT = 'Warning: phone does not valid!';\n" +
        "        END IF;\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER user_phone_validation BEFORE INSERT ON user\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        IF (NEW.user_phone  REGEXP '[0-9]{11}') = 0 THEN\n" +
        "            SIGNAL SQLSTATE '02000' SET MESSAGE_TEXT = 'Warning: phone does not valid!';\n" +
        "        END IF;\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER address_phone_validation BEFORE INSERT ON address\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        IF (NEW.address_phone  REGEXP '[0-9]{11}') = 0 THEN\n" +
        "            SIGNAL SQLSTATE '02000' SET MESSAGE_TEXT = 'Warning: phone does not valid!';\n" +
        "        END IF;\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER courier_phone_validation BEFORE INSERT ON courier\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        IF (NEW.courier_phone  REGEXP '[0-9]{11}') = 0 THEN\n" +
        "            SIGNAL SQLSTATE '02000' SET MESSAGE_TEXT = 'Warning: phone does not valid!';\n" +
        "        END IF;\n" +
        "    end;\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "create table Log_Courier\n" +
        "(\n" +
        "\tLog_ID integer auto_increment,\n" +
        "\tLog_time datetime default NOW() null,\n" +
        "\tLog_type varchar(6) not null,\n" +
        "\tcourier_ID integer null,\n" +
        "\tcourier_name varchar(30) null,\n" +
        "\tcourier_family varchar(30) null,\n" +
        "\tcourier_phone varchar(11) null,\n" +
        "\tconstraint Log_Courier_pk\n" +
        "\t\tprimary key (Log_ID)\n" +
        ");\n" +
        "\n" +
        "CREATE TRIGGER log_insert_courier AFTER INSERT ON courier\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO log_courier(log_type, courier_id, courier_name, courier_family, courier_phone)\n" +
        "        VALUES ('insert',NEW.courier_ID,NEW.courier_name,NEW.courier_family,NEW.courier_phone);\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER log_delete_courier AFTER DELETE ON courier\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO log_courier(log_type, courier_id, courier_name, courier_family, courier_phone)\n" +
        "        VALUES ('delete',OLD.courier_ID,OLD.courier_name,OLD.courier_family,OLD.courier_phone);\n" +
        "    end;\n" +
        "\n" +
        "\n" +
        "CREATE TRIGGER log_update_courier AFTER UPDATE ON courier\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO log_courier(log_type, courier_id, courier_name, courier_family, courier_phone)\n" +
        "        VALUES ('update',NEW.courier_ID,NEW.courier_name,NEW.courier_family,NEW.courier_phone);\n" +
        "    end;\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "create table Log_Address\n" +
        "(\n" +
        "\tLog_ID integer auto_increment,\n" +
        "\tLog_time datetime default NOW() null,\n" +
        "\tLog_type varchar(6) not null,\n" +
        "\taddress_name varchar(30) null,\n" +
        "\taddress_location varchar(100) null,\n" +
        "\taddress_phone varchar(11),\n" +
        "\tuser_ID integer null,\n" +
        "\n" +
        "\tconstraint Log_Address_pk\n" +
        "\t\tprimary key (Log_ID)\n" +
        ");\n" +
        "\n" +
        "\n" +
        "CREATE TRIGGER log_insert_address AFTER INSERT ON address\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_Address(log_type,address_name,address_location,address_phone,user_ID)\n" +
        "        VALUES ('insert',NEW.address_name,NEW.address_location,NEW.address_phone,NEW.user_ID);\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER log_delete_address AFTER DELETE ON address\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_Address(log_type,address_name,address_location,address_phone,user_ID)\n" +
        "        VALUES ('delete',OLD.address_name,OLD.address_location,OLD.address_phone,OLD.user_ID);\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER log_update_address AFTER UPDATE ON address\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "\t\tINSERT INTO Log_Address(log_type,address_name,address_location,address_phone,user_ID)\n" +
        "        VALUES ('update',NEW.address_name,NEW.address_location,NEW.address_phone,NEW.user_ID);\n" +
        "    end;\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "create table Log_Item\n" +
        "(\n" +
        "\tLog_ID integer auto_increment,\n" +
        "\tLog_time datetime default NOW() null,\n" +
        "\tLog_type varchar(6) not null,\n" +
        "\titem_ID integer null,\n" +
        "\titem_name varchar(30) null,\n" +
        "\titem_price integer null,\n" +
        "\tconstraint Log_Item_pk\n" +
        "\t\tprimary key (Log_ID)\n" +
        ");\n" +
        "\n" +
        "CREATE TRIGGER log_insert_item AFTER INSERT ON item\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_Item(log_type,item_ID,item_name,item_price)\n" +
        "        VALUES ('insert', NEW.item_ID,NEW.item_name,NEW.item_price);\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER log_delete_item AFTER DELETE ON item\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_Item(log_type,item_ID,item_name,item_price)\n" +
        "        VALUES ('delete',OLD.item_ID,OLD.item_name,OLD.item_price);\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER log_update_item AFTER UPDATE ON item\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_Item(log_type,item_ID,item_name,item_price)\n" +
        "        VALUES ('update',NEW.item_ID,NEW.item_name,NEW.item_price);\n" +
        "    end;\n" +
        "\t\n" +
        "\t\n" +
        "\t\n" +
        "\t\n" +
        "create table Log_Market\n" +
        "(\n" +
        "\tLog_ID integer auto_increment,\n" +
        "\tLog_time datetime default NOW() null,\n" +
        "\tLog_type varchar(6) not null,\n" +
        "    market_ID integer null ,\n" +
        "    market_name varchar(30) null,\n" +
        "    market_phone varchar(11) null,\n" +
        "    market_status boolean,\n" +
        "\tconstraint Log_Market_pk\n" +
        "\t\tprimary key (Log_ID)\n" +
        ");\n" +
        "\n" +
        "CREATE TRIGGER log_insert_market AFTER INSERT ON market\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_Market(log_type,market_ID,market_name,market_phone,market_status)\n" +
        "        VALUES ('insert', NEW.market_ID,NEW.market_name,NEW.market_phone,NEW.market_status);\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER log_delete_market AFTER DELETE ON market\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_Market(log_type,market_ID,market_name,market_phone,market_status)\n" +
        "        VALUES ('delete', OLD.market_ID,OLD.market_name,OLD.market_phone,OLD.market_status);\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER log_update_market AFTER UPDATE ON market\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_Market(log_type,market_ID,market_name,market_phone,market_status)\n" +
        "        VALUES ('update', NEW.market_ID,NEW.market_name,NEW.market_phone,NEW.market_status);\n" +
        "    end;\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "create table Log_MarketFactor\n" +
        "(\n" +
        "\tLog_ID integer auto_increment,\n" +
        "\tLog_time datetime default NOW() null,\n" +
        "\tLog_type varchar(6) not null,\n" +
        "    marketFactor_ID integer null,\n" +
        "    marketFactor_date datetime null,\n" +
        "    market_ID integer null,\n" +
        "\tconstraint Log_MarketFactor_pk\n" +
        "\t\tprimary key (Log_ID)\n" +
        ");\n" +
        "\n" +
        "\n" +
        "CREATE TRIGGER log_insert_marketFactor AFTER INSERT ON marketfactor\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_MarketFactor(log_type,marketFactor_ID,marketFactor_date,market_ID)\n" +
        "        VALUES ('insert', NEW.marketFactor_ID,NEW.marketFactor_date,NEW.market_ID);\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER log_delete_marketFactor AFTER DELETE ON marketfactor\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_MarketFactor(log_type,marketFactor_ID,marketFactor_date,market_ID)\n" +
        "        VALUES ('delete', OLD.marketFactor_ID,OLD.marketFactor_date,OLD.market_ID);\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER log_update_marketFactor AFTER UPDATE ON marketfactor\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_MarketFactor(log_type,marketFactor_ID,marketFactor_date,market_ID)\n" +
        "        VALUES ('update', NEW.marketFactor_ID,NEW.marketFactor_date,NEW.market_ID);\n" +
        "    end;\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "create table Log_MarketFactor_Item\n" +
        "(\n" +
        "\tLog_ID integer auto_increment,\n" +
        "\tLog_time datetime default NOW() null,\n" +
        "\tLog_type varchar(6) not null,\n" +
        "    marketFactor_ID integer null,\n" +
        "    item_ID integer null,\n" +
        "    marketFactor_item_number integer null,\n" +
        "    constraint Log_MarketFactor_Item_pk\n" +
        "\t\tprimary key (Log_ID)\n" +
        ");\n" +
        "\n" +
        "\n" +
        "CREATE TRIGGER log_insert_marketFactor_Item AFTER INSERT ON marketfactor_item\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_MarketFactor_Item(log_type,marketFactor_ID,item_ID,marketFactor_item_number)\n" +
        "        VALUES ('insert', NEW.marketFactor_ID,NEW.item_ID,NEW.marketFactor_Item_number);\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER log_delete_marketFactor_Item AFTER DELETE ON marketfactor_item\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_MarketFactor_Item(log_type,marketFactor_ID,item_ID,marketFactor_item_number)\n" +
        "        VALUES ('delete',  OLD.marketFactor_ID,OLD.item_ID,OLD.marketFactor_Item_number);\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER log_update_marketFactor_Item AFTER UPDATE ON marketfactor_item\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_MarketFactor_Item(log_type,marketFactor_ID,item_ID,marketFactor_item_number)\n" +
        "        VALUES ('update',  NEW.marketFactor_ID,NEW.item_ID,NEW.marketFactor_Item_number);\n" +
        "    end;\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "create table Log_Menu\n" +
        "(\n" +
        "\tLog_ID integer auto_increment,\n" +
        "\tLog_time datetime default NOW() null,\n" +
        "\tLog_type varchar(6) not null,\n" +
        "    position integer null,\n" +
        "    item_ID INTEGER null,\n" +
        "    constraint Log_Menu_pk\n" +
        "    primary key (Log_ID)\n" +
        ");\n" +
        "\n" +
        "\n" +
        "CREATE TRIGGER log_insert_menu AFTER INSERT ON menu\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_Menu(log_type,position,item_ID)\n" +
        "        VALUES ('insert', NEW.position,NEW.item_ID);\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER log_delete_menu AFTER DELETE ON menu\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_Menu(log_type,position,item_ID)\n" +
        "        VALUES ('delete',  OLD.position,OLD.item_ID);\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER log_update_menu AFTER UPDATE ON menu\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_Menu(log_type,position,item_ID)\n" +
        "        VALUES ('update', NEW.position,NEW.item_ID);\n" +
        "    end;\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "create table Log_User\n" +
        "(\n" +
        "\tLog_ID integer auto_increment,\n" +
        "\tLog_time datetime default NOW() null,\n" +
        "\tLog_type varchar(6) not null,\n" +
        "    user_ID integer null,\n" +
        "    user_name varchar(30) null,\n" +
        "    user_family varchar(30) null,\n" +
        "    user_phone varchar(11) null,\n" +
        "    user_age integer null,\n" +
        "    constraint Log_User_pk\n" +
        "    primary key (Log_ID)\n" +
        ");\n" +
        "\n" +
        "\n" +
        "CREATE TRIGGER log_insert_user AFTER INSERT ON user\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_User(log_type,user_ID,user_name,user_family,user_phone,user_age)\n" +
        "        VALUES ('insert', NEW.user_ID,NEW.user_name,NEW.user_family,NEW.user_phone,NEW.user_age);\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER log_delete_user AFTER DELETE ON user\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_User(log_type,user_ID,user_name,user_family,user_phone,user_age)\n" +
        "        VALUES ('delete', OLD.user_ID,OLD.user_name,OLD.user_family,OLD.user_phone,OLD.user_age);\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER log_update_user AFTER UPDATE ON user\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_User(log_type,user_ID,user_name,user_family,user_phone,user_age)\n" +
        "        VALUES ('update', NEW.user_ID,NEW.user_name,NEW.user_family,NEW.user_phone,NEW.user_age);\n" +
        "    end;\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "create table Log_userFactor\n" +
        "(\n" +
        "\tLog_ID integer auto_increment,\n" +
        "\tLog_time datetime default NOW() null,\n" +
        "\tLog_type varchar(6) not null,\n" +
        "    userFactor_ID integer null,\n" +
        "    userFactor_date datetime null,\n" +
        "    user_ID integer null,\n" +
        "    address_name varchar(30) null,\n" +
        "    courier_ID integer,\n" +
        "    constraint Log_userFactor_pk\n" +
        "    primary key (Log_ID)\n" +
        ");\n" +
        "\n" +
        "\n" +
        "CREATE TRIGGER log_insert_userFactor AFTER INSERT ON userfactor\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_userFactor(log_type,userFactor_ID,userFactor_date,user_ID,address_name,courier_ID)\n" +
        "        VALUES ('insert', NEW.userFactor_ID,NEW.userFactor_data,NEW.user_ID,NEW.address_name,NEW.courier_ID);\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER log_delete_userFactor AFTER DELETE ON userfactor\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_userFactor(log_type,userFactor_ID,userFactor_date,user_ID,address_name,courier_ID)\n" +
        "        VALUES ('delete', OLD.userFactor_ID,OLD.userFactor_data,OLD.user_ID,OLD.address_name,OLD.courier_ID);\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER log_update_userFactor AFTER UPDATE ON userfactor\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_userFactor(log_type,userFactor_ID,userFactor_date,user_ID,address_name,courier_ID)\n" +
        "        VALUES ('update', NEW.userFactor_ID,NEW.userFactor_data,NEW.user_ID,NEW.address_name,NEW.courier_ID);\n" +
        "    end;\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "\n" +
        "create table Log_userFactor_Item\n" +
        "(\n" +
        "\tLog_ID integer auto_increment,\n" +
        "\tLog_time datetime default NOW() null,\n" +
        "\tLog_type varchar(6) not null,\n" +
        "    userFactor_ID integer null,\n" +
        "    item_ID integer null,\n" +
        "    userFactor_Item_number integer,\n" +
        "    constraint Log_userFactor_Item_pk\n" +
        "    primary key (Log_ID)\n" +
        ");\n" +
        "\n" +
        "\n" +
        "CREATE TRIGGER log_insert_userFactor_item AFTER INSERT ON userfactor_item\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_userFactor_Item(log_type,userFactor_ID,item_ID,userFactor_Item_number)\n" +
        "        VALUES ('insert', NEW.userFactor_ID,NEW.item_ID,NEW.userFactor_Item_number);\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER log_delete_userFactor_item AFTER DELETE ON userfactor_item\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_userFactor_Item(log_type,userFactor_ID,item_ID,userFactor_Item_number)\n" +
        "        VALUES ('delete', OLD.userFactor_ID,OLD.item_ID,OLD.userFactor_Item_number);\n" +
        "    end;\n" +
        "\n" +
        "CREATE TRIGGER log_update_userFactor_item AFTER UPDATE ON userfactor_item\n" +
        "    FOR EACH ROW\n" +
        "    BEGIN\n" +
        "        INSERT INTO Log_userFactor_Item(log_type,userFactor_ID,item_ID,userFactor_Item_number)\n" +
        "        VALUES ('update', NEW.userFactor_ID,NEW.item_ID,NEW.userFactor_Item_number);\n" +
        "    end;\n" +
        "\n" +
        "create procedure removeOldLog()\n" +
        "BEGIN\n" +
        "    DELETE FROM log_address WHERE Log_time + INTERVAL 3 DAY < NOW();\n" +
        "    DELETE FROM log_courier WHERE Log_time + INTERVAL 3 DAY < NOW();\n" +
        "    DELETE FROM log_item WHERE Log_time + INTERVAL 3 DAY < NOW();\n" +
        "    DELETE FROM log_market WHERE Log_time + INTERVAL 3 DAY < NOW();\n" +
        "    DELETE FROM log_marketfactor WHERE Log_time + INTERVAL 3 DAY < NOW();\n" +
        "    DELETE FROM log_marketfactor_item WHERE Log_time + INTERVAL 3 DAY < NOW();\n" +
        "    DELETE FROM log_menu WHERE Log_time + INTERVAL 3 DAY < NOW();\n" +
        "    DELETE FROM log_user WHERE Log_time + INTERVAL 3 DAY < NOW();\n" +
        "    DELETE FROM log_userfactor WHERE Log_time + INTERVAL 3 DAY < NOW();\n" +
        "    DELETE FROM log_userfactor_item WHERE Log_time + INTERVAL 3 DAY < NOW();\n" +
        "\n" +
        "end;\n";
    }
}
