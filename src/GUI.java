/*
Import From :
https://www.logicbig.com/tutorials/java-swing/jtable-as-editable-list.html

 */

import javafx.util.Pair;

import javax.print.DocFlavor;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;

public class GUI {
    private JFrame frame;

    private JPanel buyPanel;
    private JPanel userPanel;
    private JPanel addressPanel;
    private JPanel courierPanel;
    private JPanel menuPanel;
    private JPanel reportPanel;
    private JPanel marketPanel;
    private JPanel marketFactorPanel;
    private JPanel advancePanel;
    private DatabaseInterface database;

    public GUI(DatabaseInterface database){
        this.database = database;
        makeFrame();
        makeBuyPanel();
        makeUserPanel();
        makeAddressPanel();
        makeCourierPanel();
        makeMenuPanel();
        makeReportPanel();
        makeMarketPanel();
        makeMarketFactorPanel();
        makeAdvancePanel();
        makeTabbedPane();
        frame.setVisible(true);

    }

    private void makeFrame(){
        frame = new JFrame("رستوران سیتادو");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocation(400,100);
        frame.setSize(500,600);

        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e) {
            // If Nimbus is not available, you can set the GUI to another look and feel.
        }
    }

    private void makeBuyPanel(){
        buyPanel = new JPanel(new BorderLayout());

        TableAsEditableList table = new TableAsEditableList();
        JPanel informationBuyPanel = new JPanel();
        JScrollPane scrollPane = new JScrollPane(table);

        informationBuyPanel.setLayout(new GridLayout(4,1));

        JPanel nameBuyPanel = new JPanel();
        nameBuyPanel.setLayout( new FlowLayout(FlowLayout.RIGHT) );
        nameBuyPanel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        JCheckBox nameStatusBuy = new JCheckBox("سفارش دارای نام");
        JLabel userIDBuyLabel = new JLabel("    کدملی مشتری : ");
        JComboBox<Integer> userIDBuyInput = new JComboBox<Integer>();

        nameBuyPanel.add(nameStatusBuy);
        nameBuyPanel.add(userIDBuyLabel);
        nameBuyPanel.add(userIDBuyInput);
        informationBuyPanel.add(nameBuyPanel);


        JPanel addressBuyPanel = new JPanel();
        addressBuyPanel.setLayout( new FlowLayout(FlowLayout.RIGHT));
        addressBuyPanel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        JCheckBox outStatusBuy = new JCheckBox("غذا بیرون بر");
        JLabel addressNameBuyLabel = new JLabel("    نام آدرس : ");
        JComboBox addressNameBuyInput = new JComboBox<String>();

        addressBuyPanel.add(outStatusBuy);
        addressBuyPanel.add(addressNameBuyLabel);
        addressBuyPanel.add(addressNameBuyInput);
        informationBuyPanel.add(addressBuyPanel);

        JPanel courierBuyPanel = new JPanel();
        courierBuyPanel.setLayout( new FlowLayout(FlowLayout.RIGHT));
        courierBuyPanel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        JLabel courierIDBuyLabel = new JLabel("    کدملی پیک");
        JComboBox courierIDBuyInput = new JComboBox<Integer>();
        courierBuyPanel.add(courierIDBuyLabel);
        courierBuyPanel.add(courierIDBuyInput);
        informationBuyPanel.add(courierBuyPanel);

        JButton confirmButtonBuy = new JButton("ثبت سفارش");
        informationBuyPanel.add(confirmButtonBuy);

        buyPanel.add(informationBuyPanel, BorderLayout.SOUTH);
        buyPanel.add(scrollPane,BorderLayout.CENTER);


        confirmButtonBuy.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    ArrayList<String> itemNames = new ArrayList<>();
                    ArrayList<Integer> itemNumbers = new ArrayList<>();
                    for(int i=0 ; i<table.getModel().getRowCount() ; i++){
                        String string = (String) table.getModel().getValueAt(i,0);
                        if (string.equals("")){
                            continue;
                        }
                        int stringEqualIndex = string.indexOf('=');
                        if (stringEqualIndex>=0){
                            itemNames.add(string.substring(0,stringEqualIndex));
                            itemNumbers.add(Integer.valueOf(string.substring(stringEqualIndex+1,string.length())));
                        }
                        else{
                            itemNames.add(string);
                            itemNumbers.add(1);
                        }
                    }
                    System.out.println("=======");
                    for (int i=0 ; i<itemNames.size() ; i++){
                        System.out.println(itemNames.get(i)+ " <> "+itemNumbers.get(i));
                    }
                    System.out.println("=======");


                    int userID = -1;
                    String nameAddress = null;
                    int courierID = -1;

                    if (nameStatusBuy.isSelected()){
                        userID = (int) userIDBuyInput.getSelectedItem();
                        if (outStatusBuy.isSelected()){
                            nameAddress = (String) addressNameBuyInput.getSelectedItem();
                            courierID = (int) courierIDBuyInput.getSelectedItem();
                        }
                    }

                    database.buyUser(userID,nameAddress,courierID,itemNames,itemNumbers);
                    JOptionPane.showMessageDialog
                            (null,"سفارش با موفقیت ثبت شد.",
                                    "عملیات موفق",JOptionPane.INFORMATION_MESSAGE);
                }
                catch (Exception ex){
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog
                            (null,"عملیات دچار مشکل شد!",
                                    "عملیات ناموفق",JOptionPane.ERROR_MESSAGE);
                }

            }

        });

        nameStatusBuy.setSelected(false);
        outStatusBuy.setSelected(false);
        userIDBuyInput.setEnabled(false);
        userIDBuyLabel.setEnabled(false);
        setEnable(addressBuyPanel, false);
        setEnable(courierBuyPanel,false);

        nameStatusBuy.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                outStatusBuy.setSelected(false);
                if (nameStatusBuy.isSelected()){
                    userIDBuyInput.setEnabled(true);
                    userIDBuyLabel.setEnabled(true);
                    outStatusBuy.setEnabled(true);
                    addressNameBuyInput.setEnabled(false);
                    addressNameBuyLabel.setEnabled(false);
                    setEnable(courierBuyPanel,false);

                    userIDBuyInput.removeAllItems();
                    for (Integer userId : database.allUserID()){
                        userIDBuyInput.addItem(userId);
                    }
                    userIDBuyInput.setSelectedIndex(0);
                }
                else{
                    userIDBuyInput.setEnabled(false);
                    userIDBuyLabel.setEnabled(false);
                    setEnable(addressBuyPanel, false);
                    setEnable(courierBuyPanel,false);

                }
            }
        });

        outStatusBuy.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                courierIDBuyInput.removeAllItems();
                for (Integer courierId : database.allCourierID()){
                    courierIDBuyInput.addItem(courierId);
                }
                courierIDBuyInput.setSelectedIndex(0);

                if (outStatusBuy.isSelected()){
                    addressNameBuyInput.setEnabled(true);
                    addressNameBuyLabel.setEnabled(true);
                    setEnable(courierBuyPanel,true);
                }
                else{
                    setEnable(courierBuyPanel,false);
                    addressNameBuyInput.setEnabled(false);
                    addressNameBuyLabel.setEnabled(false);
                }

            }
        });

        userIDBuyInput.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                Object selectedObject = userIDBuyInput.getSelectedItem();
                if (selectedObject!=null){
                    Integer userID = (Integer) selectedObject;
                    try {
                        addressNameBuyInput.removeAllItems();
                        for (Address address : database.allAddress(userID)){
                            addressNameBuyInput.addItem(address.getAddressName());
                        }
                        addressNameBuyInput.setSelectedIndex(0);
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
    }

    private void makeUserPanel(){
        userPanel = new JPanel();
        userPanel.setLayout(new BorderLayout());

        JPanel buttonsUserPanel = new JPanel();
        buttonsUserPanel.setLayout(new GridLayout(6,1,20,20));

        JButton addUserButton = new JButton("افزودن کاربر جدید");
        JButton removeUserButton = new JButton("حذف کاربر");
        JButton updateUserButton = new JButton("ویرایش کاربر");

        buttonsUserPanel.add(new JLabel());
        buttonsUserPanel.add(addUserButton);
        buttonsUserPanel.add(removeUserButton);
        buttonsUserPanel.add(updateUserButton);
        userPanel.add(buttonsUserPanel,BorderLayout.EAST);

        JPanel inputUserPanel = new JPanel();
        inputUserPanel.setLayout(new GridLayout(6,2,20,20));
        inputUserPanel.setBorder(BorderFactory.createBevelBorder(1));



        JLabel userIDLabel = new JLabel("    کد ملی : ");
        userIDLabel.setSize(10,50);
        userIDLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        JTextField userIDInput = new JTextField();
        JLabel userNameLabel = new JLabel("    نام : ");
        userNameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        JTextField userNameInput = new JTextField();
        JLabel userFamilyLabel = new JLabel("    نام خانوادگی : ");
        userFamilyLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        JTextField userFamilyInput = new JTextField();
        JLabel userPhoneLabel = new JLabel("    شماره تلفن : ");
        userPhoneLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        JTextField userPhoneInput = new JTextField();
        JLabel userAgeLabel = new JLabel("    سن : ");
        userAgeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        JTextField userAgeInput = new JTextField();
        JButton confirmButton = new JButton("    تایید");

        inputUserPanel.add(userIDInput);
        inputUserPanel.add(userIDLabel);
        inputUserPanel.add(userNameInput);
        inputUserPanel.add(userNameLabel);
        inputUserPanel.add(userFamilyInput);
        inputUserPanel.add(userFamilyLabel);
        inputUserPanel.add(userPhoneInput);
        inputUserPanel.add(userPhoneLabel);
        inputUserPanel.add(userAgeInput);
        inputUserPanel.add(userAgeLabel);
        inputUserPanel.add(confirmButton);
        userPanel.add(inputUserPanel,BorderLayout.CENTER);
        setEnable(inputUserPanel,false);
        updateUserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int user_ID = Integer.parseInt(JOptionPane.showInputDialog
                        (null, "کد ملی کاربر مورد نظر را وارد کنید:","ویرایش مشخصات کاربر",JOptionPane.QUESTION_MESSAGE));

                User user = database.user(user_ID);
                if (user == null){
                    JOptionPane.showMessageDialog
                            (null," کاربری با چنین کدملی وجود ندارد!",
                                    "خطا",JOptionPane.ERROR_MESSAGE);
                }
                else{
                    setEnable(inputUserPanel,true);
                    userIDLabel.setEnabled(false);
                    userIDInput.setEnabled(false);

                    userIDInput.setText(String.valueOf(user.getUserID()));
                    userNameInput.setText(user.getUserName());
                    userFamilyInput.setText(user.getUserFamily());
                    userPhoneInput.setText(user.getUserPhone());
                    userAgeInput.setText(String.valueOf(user.getUserAge()));
                }
            }
        });

        addUserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setEnable(inputUserPanel,true);

            }
        });
        removeUserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    setEnable(inputUserPanel,false);
                    int user_ID = Integer.parseInt(JOptionPane.showInputDialog
                            (null, "کد ملی کاربر مورد نظر را وارد کنید:"
                                    ,"حذف کاربر",JOptionPane.QUESTION_MESSAGE));

                    User user = database.user(user_ID);
                    if (user == null){
                        throw new Exception();
                    }
                    else{
                        database.removeUser(user_ID);
                        JOptionPane.showMessageDialog
                                (null,user.getUserName() + user.getUserFamily() + " حذف شد!",
                                        "عملیات موفق",JOptionPane.INFORMATION_MESSAGE);
                    }
                }
                catch (Exception ex){
                    JOptionPane.showMessageDialog
                            (null," کاربری با چنین کدملی وجود ندارد!",
                                    "خطا",JOptionPane.ERROR_MESSAGE);
                    ex.printStackTrace();
                }
            }
        });

        confirmButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    User user = new User(Integer.parseInt(userIDInput.getText()), userNameInput.getText(),
                            userFamilyInput.getText(), userPhoneInput.getText(), Integer.parseInt(userAgeInput.getText()));

                    boolean output;
                    if (userIDInput.isEnabled()){
                        output = database.addUser(user);
                    }
                    else{
                        output = database.updateUser(user);

                    }
                    if (!output){
                        throw new Exception();
                    }

                    if (userIDInput.isEnabled()){
                        JOptionPane.showMessageDialog
                                (null,"کاربر با موفقیت به سامانه افزوده شد.",
                                        "عملیات موفق",JOptionPane.INFORMATION_MESSAGE);
                    }
                    else{
                        JOptionPane.showMessageDialog
                                (null,"کاربر با موفقیت به روز شد.",
                                        "عملیات موفق",JOptionPane.INFORMATION_MESSAGE);

                    }
                }
                catch (Exception e1){
                    JOptionPane.showMessageDialog
                            (null,"عملیات دچار مشکل شد!\n تمامی فبلد ها باید پر و معتبر باشد.\n هر کاربر باید کدملی یکتای مربوط به خود را داشته باشد.",
                                    "عملیات ناموفق",JOptionPane.ERROR_MESSAGE);

                }
            }
        });
    }

    private void makeAddressPanel(){
        addressPanel = new JPanel();
        addressPanel.setLayout(new BorderLayout());

        JPanel buttonsAddressPanel = new JPanel();
        buttonsAddressPanel.setLayout(new GridLayout(6,1,20,20));

        JButton addAddressButton = new JButton("افزودن آدرس جدید");
        JButton removeAddressButton = new JButton("حذف آدرس");
        JButton updateAddressButton = new JButton("ویرایش آدرس");

        buttonsAddressPanel.add(new JLabel());
        buttonsAddressPanel.add(addAddressButton);
        buttonsAddressPanel.add(removeAddressButton);
        buttonsAddressPanel.add(updateAddressButton);
        addressPanel.add(buttonsAddressPanel,BorderLayout.EAST);

        JPanel inputAddressPanel = new JPanel();
        inputAddressPanel.setLayout(new GridLayout(5,2,20,20));
        inputAddressPanel.setBorder(BorderFactory.createBevelBorder(1));

        JLabel userIDLabel = new JLabel("    کدملی صاحب آدرس : ");
        userIDLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        JTextField userIDInput = new JTextField();
        JLabel addressNameLabel = new JLabel("    نام آدرس : ");
        addressNameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        JTextField addressNameInput = new JTextField();
        JLabel addressLocationLabel = new JLabel("    مکان آدرس : ");
        addressLocationLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        JTextArea addressLocationInput = new JTextArea();
        JLabel addressPhoneLabel = new JLabel("    شماره تلفن ثابت : ");
        addressPhoneLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        JTextField addressPhoneInput = new JTextField();
        JButton confirmAddressButton = new JButton("تایید");

        inputAddressPanel.add(userIDInput);
        inputAddressPanel.add(userIDLabel);
        inputAddressPanel.add(addressNameInput);
        inputAddressPanel.add(addressNameLabel);
        inputAddressPanel.add(addressLocationInput);
        inputAddressPanel.add(addressLocationLabel);
        inputAddressPanel.add(addressPhoneInput);
        inputAddressPanel.add(addressPhoneLabel);
        inputAddressPanel.add(confirmAddressButton);
        addressPanel.add(inputAddressPanel,BorderLayout.CENTER);
        setEnable(inputAddressPanel,false);

        confirmAddressButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    int userID = Integer.parseInt(userIDInput.getText());
                    String addressName = addressNameInput.getText();
                    String addressLocation = addressLocationInput.getText();
                    String addressPhone = addressPhoneInput.getText();

                    if (userIDInput.isEnabled()){
                        database.addAddress(addressName,addressLocation,addressPhone,userID);
                        JOptionPane.showMessageDialog
                                (null,"آدرس با موفقیت به سامانه افزوده شد.",
                                        "عملیات موفق",JOptionPane.INFORMATION_MESSAGE);

                    }
                    else{
                        Address address = new Address(userID,addressName,addressLocation,addressPhone);
                        database.updateAddress(address);
                        JOptionPane.showMessageDialog
                                (null,"آدرس با موفقیت به روز شد.",
                                        "عملیات موفق",JOptionPane.INFORMATION_MESSAGE);

                    }

                }
                catch (Exception ex){
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog
                            (null,"عملیات دچار مشکل شد!",
                                    "عملیات ناموفق",JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        addAddressButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setEnable(inputAddressPanel,true);
            }
        });

        updateAddressButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int user_ID = Integer.parseInt(JOptionPane.showInputDialog
                            (null, "کد ملی کاربر مورد نظر را وارد کنید:", "ویرایش مشخصات آدرس", JOptionPane.QUESTION_MESSAGE));
                    User user = database.user(user_ID);
                    if (user == null){
                        throw new Exception();
                    }

                    String address_name = JOptionPane.showInputDialog
                            (null, "نام آدرس مورد نظر را وارد کنید:", "ویرایش مشخصات آدرس", JOptionPane.QUESTION_MESSAGE);

                    Address address = database.address(user_ID,address_name);
                    userIDInput.setText(String.valueOf(address.getUserID()));
                    addressNameInput.setText(address.getAddressName());
                    addressLocationInput.setText(address.getAddressLocation());
                    addressPhoneInput.setText(address.getAddressPhone());


                    setEnable(inputAddressPanel, true);
                    userIDInput.setEnabled(false);
                    userIDLabel.setEnabled(false);
                    addressNameInput.setEnabled(false);
                    addressNameLabel.setEnabled(false);
                }
                catch (Exception ex){
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog
                            (null,"عملیات دچار مشکل شد!",
                                    "عملیات ناموفق",JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        removeAddressButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int user_ID = Integer.parseInt(JOptionPane.showInputDialog
                            (null, "کد ملی کاربر مورد نظر را وارد کنید:", "ویرایش مشخصات آدرس", JOptionPane.QUESTION_MESSAGE));
                    User user = database.user(user_ID);
                    if (user == null){
                        throw new Exception();
                    }

                    String address_name = JOptionPane.showInputDialog
                            (null, "نام آدرس مورد نظر را وارد کنید:", "ویرایش مشخصات آدرس", JOptionPane.QUESTION_MESSAGE);

                    database.removeAddress(user_ID,address_name);

                    JOptionPane.showMessageDialog
                            (null,"آدرس با موفقیت حذف شد.",
                                    "عملیات موفق",JOptionPane.INFORMATION_MESSAGE);
                }
                catch (Exception ex){
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog
                            (null,"عملیات دچار مشکل شد!",
                                    "عملیات ناموفق",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    private void makeCourierPanel(){
        courierPanel = new JPanel();
        courierPanel.setLayout(new BorderLayout());

        JPanel buttonsCourierPanel = new JPanel();
        buttonsCourierPanel.setLayout(new GridLayout(6,1,20,20));

        JButton addCourierButton = new JButton("افزودن پیک جدید");
        JButton removeCourierButton = new JButton("حذف پیک");
        JButton updateCourierButton = new JButton("ویرایش پیک");
        buttonsCourierPanel.add(new JLabel());
        buttonsCourierPanel.add(addCourierButton);
        buttonsCourierPanel.add(removeCourierButton);
        buttonsCourierPanel.add(updateCourierButton);
        courierPanel.add(buttonsCourierPanel,BorderLayout.EAST);

        JPanel inputCourierPanel = new JPanel();
        inputCourierPanel.setLayout(new GridLayout(5,2,20,20));
        inputCourierPanel.setBorder(BorderFactory.createBevelBorder(1));

        JLabel courierIDLabel = new JLabel("    کدملی: ");
        courierIDLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        JTextField courierIDInput = new JTextField();
        JLabel courierNameLabel = new JLabel("    نام : ");
        courierNameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        JTextField courierNameInput = new JTextField();
        JLabel courierFamilyLabel = new JLabel("    نام خانوادگی : ");
        courierFamilyLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        JTextField courierFamilyInput = new JTextField();
        JLabel courierPhoneLabel = new JLabel("    تلفن : ");
        courierPhoneLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        JTextField courierPhoneInput = new JTextField();
        JButton confirmButtonCourier = new JButton("تایید");

        inputCourierPanel.add(courierIDInput);
        inputCourierPanel.add(courierIDLabel);
        inputCourierPanel.add(courierNameInput);
        inputCourierPanel.add(courierNameLabel);
        inputCourierPanel.add(courierFamilyInput);
        inputCourierPanel.add(courierFamilyLabel);
        inputCourierPanel.add(courierPhoneInput);
        inputCourierPanel.add(courierPhoneLabel);
        inputCourierPanel.add(confirmButtonCourier);

        courierPanel.add(inputCourierPanel,BorderLayout.CENTER);
        setEnable(inputCourierPanel,false);
        
        confirmButtonCourier.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int courierID = Integer.parseInt(courierIDInput.getText());
                    String courierName = courierNameInput.getText();
                    String courierFamily = courierFamilyInput.getText();
                    String courierPhone = courierPhoneInput.getText();
                    Courier courier = new Courier(courierID, courierName, courierFamily, courierPhone);

                    if (courierIDInput.isEnabled()) {
                        database.addCourier(courier);

                        JOptionPane.showMessageDialog
                                (null,"پیک با موفقیت به سامانه افزوده شد.",
                                        "عملیات موفق",JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        database.updateCourier(courier);
                        JOptionPane.showMessageDialog
                                (null,"پیک با موفقیت به روز شد.",
                                        "عملیات موفق",JOptionPane.INFORMATION_MESSAGE);
                    }

                }
                catch (Exception ex){
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog
                            (null,"عملیات دچار مشکل شد!",
                                    "عملیات ناموفق",JOptionPane.ERROR_MESSAGE);

                }
            }
        });
        
        addCourierButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setEnable(inputCourierPanel,true);
            }
        });
        
        updateCourierButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    int courier_ID = Integer.parseInt(JOptionPane.showInputDialog
                            (null, "کد ملی پیک مورد نظر را وارد کنید:", "ویرایش مشخصات پیک", JOptionPane.QUESTION_MESSAGE));
                    Courier courier = database.courier(courier_ID);
                    courierIDInput.setText(String.valueOf(courier.getCourierID()));
                    courierNameInput.setText(courier.getCourierName());
                    courierFamilyInput.setText(courier.getCourierFamily());
                    courierPhoneInput.setText(courier.getCourierPhone());


                    setEnable(inputCourierPanel,true);
                    courierIDInput.setEnabled(false);
                    courierIDInput.setEnabled(false);
                }
                catch (Exception ex){
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog
                            (null,"عملیات دچار مشکل شد!",
                                    "عملیات ناموفق",JOptionPane.ERROR_MESSAGE);
                }
                
                
            }
        });
        
        removeCourierButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int courier_ID = Integer.parseInt(JOptionPane.showInputDialog
                            (null, "کد ملی پیک مورد نظر را وارد کنید:", "ویرایش مشخصات آدرس", JOptionPane.QUESTION_MESSAGE));
                    database.removeCourier(courier_ID);
                    setEnable(inputCourierPanel,false);


                    JOptionPane.showMessageDialog
                            (null,"پیک با موفقیت حذف شد.",
                                    "عملیات موفق",JOptionPane.INFORMATION_MESSAGE);
                }
                catch (Exception ex){
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog
                            (null,"عملیات دچار مشکل شد!",
                                    "عملیات ناموفق",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    private void makeMenuPanel(){
        menuPanel = new JPanel(new BorderLayout());



        try {
            JTable tableMenu = new JTable();
            DefaultTableModel modelMenu = new DefaultTableModel();
            modelMenu.addColumn("نام غذا");
            modelMenu.addColumn("قیمت غذا");

            ArrayList<Pair<String,Integer>> menuItems = database.menuItems();
            for (int i=0 ; i<menuItems.size() ; i++){
                modelMenu.addRow(new String[]{menuItems.get(i).getKey(), String.valueOf(menuItems.get(i).getValue())});
            }

            tableMenu.setModel(modelMenu);
            JScrollPane menuScrollPane = new JScrollPane(tableMenu);
            menuPanel.add(menuScrollPane,BorderLayout.CENTER);

            JPanel buttonsPanel = new JPanel(new GridLayout(1,3));
            JButton addRowButton = new JButton("افزودن سطر جدید به منو");
            JButton confirmButtonMenu = new JButton("تایید و به روز رسانی منو");
            JButton resetButtonMenu = new JButton("بازگرداندن منوی فعلی");
            buttonsPanel.add(addRowButton);
            buttonsPanel.add(confirmButtonMenu);
            buttonsPanel.add(resetButtonMenu);
            menuPanel.add(buttonsPanel,BorderLayout.SOUTH);

            addRowButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    modelMenu.addRow(new String[]{"", ""});
                }
            });

            confirmButtonMenu.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        try {
                            tableMenu.getCellEditor().stopCellEditing();
                        }
                        catch (Exception ignored){

                        }
                        ArrayList<Pair<String, Integer>> menuItems = new ArrayList<>();
                        HashSet<String> names = new HashSet<>();
                        for (int i = 0; i < modelMenu.getRowCount(); i++) {
                            System.out.println(modelMenu.getValueAt(i, 0));
                            System.out.println(modelMenu.getValueAt(i,1));
                            System.out.println("_____________________");
                            String name = (String) modelMenu.getValueAt(i, 0);
                            if (!name.equals("")) {
                                Integer price = Integer.parseInt((String) modelMenu.getValueAt(i, 1));

                                if (!names.contains(name)) {
                                    names.add(name);
                                    Pair<String, Integer> item = new Pair<>(name, price);
                                    menuItems.add(item);
                                }
                            }

                        }
                        database.setMenuItems(menuItems);
                        menuItems = database.menuItems();
                        modelMenu.setRowCount(0);
                        for (int i=0 ; i<menuItems.size() ; i++){
                            modelMenu.addRow(new String[]{menuItems.get(i).getKey(), String.valueOf(menuItems.get(i).getValue())});
                        }

                        JOptionPane.showMessageDialog
                                (null,"منوی رستوران با موفقیت به روز شد.",
                                        "عملیات موفق",JOptionPane.INFORMATION_MESSAGE);



                    }
                    catch (Exception ex){
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog
                                (null,"عملیات دچار مشکل شد!",
                                        "عملیات ناموفق",JOptionPane.ERROR_MESSAGE);
                    }
                }
            });

            resetButtonMenu.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        ArrayList<Pair<String, Integer>> menuItems = new ArrayList<>();

                        menuItems = database.menuItems();
                        modelMenu.setRowCount(0);
                        for (int i = 0; i < menuItems.size(); i++) {
                            modelMenu.addRow(new String[]{menuItems.get(i).getKey(), String.valueOf(menuItems.get(i).getValue())});
                        }
                    }
                    catch (Exception ex){

                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void makeReportPanel(){
        reportPanel = new JPanel(new BorderLayout());
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout( new FlowLayout(FlowLayout.RIGHT) );
        buttonsPanel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);

        JTextArea resultReportText = new JTextArea();
        JScrollPane resultReportScrollPane = new JScrollPane(resultReportText);
        resultReportText.setEditable(false);
        resultReportText.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);

        JButton userReportButton = new JButton("دریافت گزارش کاربر");
        JButton managerReportButton = new JButton("دریافت گزارش مدیر");
        buttonsPanel.add(userReportButton);
        buttonsPanel.add(managerReportButton);
        reportPanel.add(buttonsPanel,BorderLayout.NORTH);

        reportPanel.add(resultReportScrollPane,BorderLayout.CENTER);

        userReportButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    int user_ID = Integer.parseInt(JOptionPane.showInputDialog
                            (null, "کدملی مورد نظر را وارد کنید:", "گزارش کاربر", JOptionPane.QUESTION_MESSAGE));

                    int sumPrice = database.sumPricesUser(user_ID);
                    String bestName = database.bestNameUserFactor(user_ID);
                    String text = "مجموع کل خرید ها : "+sumPrice+ "\n"+"محبوب ترین غذا : " + bestName;

                    resultReportText.setText(text);
                    JOptionPane.showMessageDialog
                            (null,"'گزارش با موفقیت به دست آمد.",
                                    "عملیات موفق",JOptionPane.INFORMATION_MESSAGE);

                }
                catch (Exception ex){
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog
                            (null,"عملیات دچار مشکل شد!",
                                    "عملیات ناموفق",JOptionPane.ERROR_MESSAGE);
                }
            }

        });

        managerReportButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ArrayList<Pair<Integer,Integer>>reportUser = database.managerReportUser();
                    resultReportText.setText("");
                    resultReportText.append("فروش روزانه : ");
                    resultReportText.append("\n");
                    int sumSell = 0, sumBuy = 0 ;
                    for(Pair<Integer,Integer> pii : reportUser){
                        resultReportText.append(pii.getKey() + " ) " + pii.getValue() + "\n");
                        sumSell += pii.getValue();
                    }
                    resultReportText.append("=============================");
                    resultReportText.append("\n");
                    ArrayList<Pair<Integer,Integer>>reportMarket = database.managerReportMarket();
                    resultReportText.append("خرید روزانه : ");
                    resultReportText.append("\n");
                    for(Pair<Integer,Integer> pii : reportMarket){
                        resultReportText.append(pii.getKey() + " ) " + pii.getValue() + "\n");
                        sumBuy += pii.getValue();
                    }
                    resultReportText.append("=============================");
                    resultReportText.append("\n");
                    resultReportText.append("مجموع فروش روزانه : " + sumSell);
                    resultReportText.append("\n");
                    resultReportText.append("مجموع خرید روزانه : " + sumBuy);
                    resultReportText.append("\n");
                    resultReportText.append("سود خالص روزانه : " + (sumSell - sumBuy));
                    resultReportText.append("\n");
                    JOptionPane.showMessageDialog
                            (null,"'گزارش با موفقیت به دست آمد.",
                                    "عملیات موفق",JOptionPane.INFORMATION_MESSAGE);
                } catch (Exception e1) {
                    e1.printStackTrace();
                    JOptionPane.showMessageDialog
                            (null,"عملیات دچار مشکل شد!",
                                    "عملیات ناموفق",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    private void makeMarketPanel(){
        marketPanel = new JPanel();
        marketPanel.setLayout(new BorderLayout());

        JPanel buttonsMarketPanel = new JPanel();
        buttonsMarketPanel.setLayout(new GridLayout(6,1,20,20));

        JButton addMarketButton = new JButton("افزودن فروشگاه جدید");
        JButton updateMarketButton = new JButton("ویرایش فروشگاه");
        buttonsMarketPanel.add(new JLabel());
        buttonsMarketPanel.add(addMarketButton);
        buttonsMarketPanel.add(updateMarketButton);
        marketPanel.add(buttonsMarketPanel,BorderLayout.EAST);


        JPanel inputMarketPanel = new JPanel();
        inputMarketPanel.setLayout(new GridLayout(5,2,20,20));
        inputMarketPanel.setBorder(BorderFactory.createBevelBorder(1));

        JLabel marketIDLabel = new JLabel("    شماره ثبت: ");
        marketIDLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        JTextField marketIDInput = new JTextField();
        JLabel marketNameLabel = new JLabel("    نام فروشگاه: ");
        marketNameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        JTextField marketNameInput = new JTextField();
        JLabel marketPhoneLabel = new JLabel("    تلفن: ");
        marketPhoneLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        JTextField marketPhoneInput = new JTextField();
        JCheckBox marketStatusInput = new JCheckBox(" وضعیت فعال ");
        marketStatusInput.setHorizontalAlignment(SwingConstants.RIGHT);
        JButton confirmButtonMarket = new JButton("تایید");

        inputMarketPanel.add(marketIDInput);
        inputMarketPanel.add(marketIDLabel);
        inputMarketPanel.add(marketNameInput);
        inputMarketPanel.add(marketNameLabel);
        inputMarketPanel.add(marketPhoneInput);
        inputMarketPanel.add(marketPhoneLabel);
        inputMarketPanel.add(marketStatusInput);
        inputMarketPanel.add(new JLabel());
        inputMarketPanel.add(confirmButtonMarket);
        marketPanel.add(inputMarketPanel,BorderLayout.CENTER);

        confirmButtonMarket.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int marketID= Integer.parseInt(marketIDInput.getText());
                    String marketName = marketNameInput.getText();
                    String marketPhone = marketPhoneInput.getText();
                    boolean marketStatus = marketStatusInput.isSelected();
                    Market market = new Market(marketID,marketName,marketPhone,marketStatus);

                    if (marketIDInput.isEnabled()) {
                        database.addMarket(market);

                        JOptionPane.showMessageDialog
                                (null,"فروشگاه با موفقیت به سامانه افزوده شد.",
                                        "عملیات موفق",JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        database.updateMarket(market);
                        JOptionPane.showMessageDialog
                                (null,"فروشگاه با موفقیت به روز شد.",
                                        "عملیات موفق",JOptionPane.INFORMATION_MESSAGE);
                    }

                }
                catch (Exception ex){
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog
                            (null,"عملیات دچار مشکل شد!",
                                    "عملیات ناموفق",JOptionPane.ERROR_MESSAGE);

                }
            }
        });

        updateMarketButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    int market_ID = Integer.parseInt(JOptionPane.showInputDialog
                            (null, "شماره ثبت فروشگاه مورد نظر را وارد کنید:", "ویرایش مشخصات فروشگاه", JOptionPane.QUESTION_MESSAGE));
                    Market market = database.market(market_ID);
                    marketIDInput.setText(String.valueOf(market.getMarketId()));
                    marketNameInput.setText(market.getMarketName());
                    marketPhoneInput.setText(market.getMarketPhone());
                    marketStatusInput.setSelected(market.isMarketStatus());


                    marketIDLabel.setEnabled(false);
                    marketIDInput.setEnabled(false);
                }
                catch (Exception ex){
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog
                            (null,"عملیات دچار مشکل شد!",
                                    "عملیات ناموفق",JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        addMarketButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                marketIDInput.setEnabled(true);
                marketIDLabel.setEnabled(true);
            }
        });
    }

    private void makeMarketFactorPanel(){
        marketFactorPanel = new JPanel(new BorderLayout());

        TableAsEditableList table = new TableAsEditableList();
        JPanel informationMarketPanel = new JPanel();
        JScrollPane scrollPane = new JScrollPane(table);

        informationMarketPanel.setLayout(new GridLayout(2,1));

        JPanel marketIDPanel = new JPanel();
        marketIDPanel.setLayout( new FlowLayout(FlowLayout.RIGHT) );
        marketIDPanel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        JLabel marketIDLabel = new JLabel("   شماره ثبت فروشگاه : ");
        JComboBox<Integer> marketIDInput = new JComboBox<>();
        ArrayList<Integer> marketIDs = null;
        try {
            marketIDs = database.allMarketID();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (Integer marketID : marketIDs){
            marketIDInput.addItem(marketID);
        }

        marketIDPanel.add(marketIDLabel);
        marketIDPanel.add(marketIDInput);
        informationMarketPanel.add(marketIDPanel);

        JButton confirmButtonBuy = new JButton("ثبت سفارش");
        informationMarketPanel.add(confirmButtonBuy);

        marketFactorPanel.add(informationMarketPanel, BorderLayout.SOUTH);
        marketFactorPanel.add(scrollPane,BorderLayout.CENTER);


        confirmButtonBuy.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    ArrayList<String> itemNames = new ArrayList<>();
                    ArrayList<Integer> itemNumbers = new ArrayList<>();
                    ArrayList<Integer> itemPrices = new ArrayList<>();
                    for(int i=0 ; i<table.getModel().getRowCount() ; i++){
                        String string = (String) table.getModel().getValueAt(i,0);
                        if (string.equals("")){
                            continue;
                        }
                        int stringEqualIndex = string.indexOf('=');
                        int stringDollerIndex = string.indexOf('$');
                        if (stringEqualIndex>=0){
                            itemNames.add(string.substring(0,stringEqualIndex));
                            itemPrices.add(Integer.valueOf(string.substring(stringEqualIndex+1,stringDollerIndex)));
                            itemNumbers.add(Integer.valueOf(string.substring(stringDollerIndex+1,string.length())));
                        }
                        else{
                            itemNames.add(string.substring(0,stringDollerIndex));
                            itemPrices.add(Integer.valueOf(string.substring(stringDollerIndex+1,string.length())));
                            itemNumbers.add(1);
                        }
                    }
                    System.out.println("=======");
                    for (int i=0 ; i<itemNames.size() ; i++){
                        System.out.println(itemNames.get(i)+ " <> "+itemNumbers.get(i));
                    }
                    System.out.println("=======");


                    int marketID = (int) marketIDInput.getSelectedItem();

                    database.buyMarket(marketID,itemNames,itemNumbers,itemPrices);
                    JOptionPane.showMessageDialog
                            (null,"سفارش با موفقیت ثبت شد.",
                                    "عملیات موفق",JOptionPane.INFORMATION_MESSAGE);
                }
                catch (Exception ex){
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog
                            (null,"عملیات دچار مشکل شد!",
                                    "عملیات ناموفق",JOptionPane.ERROR_MESSAGE);
                }

            }

        });
    }

    private void makeAdvancePanel(){
        advancePanel = new JPanel(new BorderLayout());

        JPanel buttonPanel = new JPanel(new FlowLayout());
        JButton removeLogButton = new JButton("حذف لاگ های قدیمی");
        JButton createDatabaseButton = new JButton("ساختن پایگاه داده");
        JButton dropDatabaseButton = new JButton("حذف پایگاه داده");
        buttonPanel.add(removeLogButton);
        buttonPanel.add(createDatabaseButton);
        buttonPanel.add(dropDatabaseButton);


        advancePanel.add(buttonPanel,BorderLayout.NORTH);
        removeLogButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    database.removeLogs();
                    System.out.println("Done");
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        createDatabaseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    database.createDatabase();
                    System.out.println("Done");
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });

        dropDatabaseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    database.removeDatabase();
                    System.out.println("Done!");
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

    private void makeTabbedPane(){
        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab("ثبت سفارش",buyPanel);
        tabbedPane.addTab("ویرایش کاربر ها",userPanel);
        tabbedPane.addTab("ویرایش آدرس ها",addressPanel);
        tabbedPane.addTab("ویرایش پیک ها",courierPanel);
        tabbedPane.addTab("وبرایش منو",menuPanel);
        tabbedPane.addTab("گزارش ها",reportPanel);
        tabbedPane.addTab("تهیه مواد اولیه",marketFactorPanel);
        tabbedPane.addTab("ویرایش فروشگاه ها",marketPanel);
        tabbedPane.addTab("تنظیمات پیشرفته",advancePanel);

        tabbedPane.setSelectedIndex(1);
        frame.add(tabbedPane);
    }

    private void setEnable(JPanel panel,boolean enable){
        for (Component component : panel.getComponents()){
            component.setEnabled(enable);
        }
    }

}
