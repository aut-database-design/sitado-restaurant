public class Address {
    private String addressName;
    private String addressLocation;
    private String addressPhone;
    private int userID;

    public Address(int userID,String addressName, String addressLocation, String addressPhone){
        this.userID = userID;
        this.addressName = addressName;
        this.addressLocation = addressLocation;
        this.addressPhone = addressPhone;
    }


    public int getUserID() {
        return userID;
    }


    public String getAddressLocation() {
        return addressLocation;
    }

    public String getAddressPhone() {
        return addressPhone;
    }

    public String getAddressName() {
        return addressName;
    }
}
