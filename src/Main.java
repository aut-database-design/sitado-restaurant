public class Main {
    public static void main(String args[]){
        DatabaseInterface dataBase= new DatabaseInterface();
        dataBase.makeConnection();

        new GUI(dataBase);

        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            public void run() {
                dataBase.closeConnection();
            }
        }));

    }






}
